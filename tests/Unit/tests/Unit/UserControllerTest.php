<?php
/**
 * Created by PhpStorm.
 * User: coqmo
 * Date: 6/4/2018
 * Time: 12:54 PM
 */

namespace tests\Unit;

use Application\Lumen53\Http\Controllers\UserController;
use Laravel\Lumen\Testing\TestCase;

class UserControllerTest extends TestCase
{

    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        return require __DIR__.'/../../../../Application/Lumen53/bootstrap/app.php';
    }


    public function testStore()
    {
        $response = $this->call('POST', '/wip/globalgaming/bookshop2/Application/Lumen53/public/api/v1/users/',['name'=>'Eric','surname'=>'Castillo','email'=>str_random(8).'@t.com']);
        $this->assertEquals(200, $response->status());

        $response = $this->call('POST', '/wip/globalgaming/bookshop2/Application/Lumen53/public/api/v1/users/',['name'=>'Eric','email'=>'psq']);
        $this->assertEquals(401, $response->status());
    }

    public function testUpdate()
    {
        $response = $this->call('POST', '/wip/globalgaming/bookshop2/Application/Lumen53/public/api/v1/users/2',['name'=>'Eric','surname'=>'Castillo','email'=>str_random(8).'@t.com']);
        $this->assertEquals(200, $response->status());

        $response = $this->call('POST', '/wip/globalgaming/bookshop2/Application/Lumen53/public/api/v1/users/2',['name'=>'Eric','email'=>'psq']);
        $this->assertEquals(401, $response->status());
    }
}
