<?php

namespace tests\Unit;



use Laravel\Lumen\Testing\TestCase;

class BookControllerTest extends TestCase
{
    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        return require __DIR__.'/../../../../Application/Lumen53/bootstrap/app.php';
    }



    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testBooksShow()
    {
        $response = $this->call('GET', '/wip/globalgaming/bookshop2/Application/Lumen53/public/api/v1/books/');
        $this->assertEquals(200, $response->status());
    }
    
}
