# Lumen-Doctrine-DDD
Domain Driven Design Book Application Example, built with Lumen 5.3 and Doctrine.



### Usage
> composer install <br />
> mv Application/Lumen53/env_example  Application/Lumen53/.env <br />
> cd Application/Lumen53/ <br />
> php artisan migrate --path="../../database/migrations/" <br />

##### API Calls
e.g. GET localhost/Lumen-Doctrine-DDD-Example/Application/Lumen53/public/api/v1/users

### Requirements
"php": ">=5.6.4", <br />
"laravel/lumen-framework": "5.3.*", <br />
"laravel-doctrine/orm": "1.2.*",
