<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DataTestSeeder extends Seeder
{
    public function __construct(){}
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $users = [
            ['name'=>'Eric','surname'=>'Castillo','email'=>'coqmos@gmail.com'],
            ['name'=>'Test 1 ','surname'=>'Castillo 1','email'=>'coqmos1@gmail.com'],
            ['name'=>'Test 2 ','surname'=>'Castillo 2','email'=>'coqmos2@gmail.com'],
            ['name'=>'Test 3 ','surname'=>'Castillo 3','email'=>'coqmos3@gmail.com'],
            ['name'=>'Test 4 ','surname'=>'Castillo 4','email'=>'coqmos4@gmail.com'],
        ];

        $books = [
            ['name'=>'Book 1','price'=>1.5,'image'=>'/test'],
            ['name'=>'Book 2','price'=>2.5,'image'=>'/test'],
            ['name'=>'Book 3','price'=>3.5,'image'=>'/test'],
            ['name'=>'Book 4','price'=>4.5,'image'=>'/test']
        ];

        DB::table('users')->insert($users);
        DB::table('books')->insert($books);



    }
}