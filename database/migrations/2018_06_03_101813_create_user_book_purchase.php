<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserBookPurchase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_book_purchase', function (Blueprint $table) {
            $table->increments('id');
            $table->addColumn('integer', 'user_id', ['unsigned' => true, 'length' => 11]);
            $table->index('user_id');
            $table->foreign('user_id')
                ->references('id')
                ->on('user');

            $table->addColumn('integer', 'book_id', ['unsigned' => true, 'length' => 11]);
            $table->index('book_id');
            $table->foreign('book_id')
                ->references('id')
                ->on('book');

            $table->timestamp('deleted_at')->nullable();
            $table->timestamp('created_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_book_purchase');
    }
}
