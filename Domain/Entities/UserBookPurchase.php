<?php
/**
 * Created by PhpStorm.
 * User: coqmo
 * Date: 6/3/2018
 * Time: 4:34 PM
 */

namespace Domain\Entities;

use \Domain\Entities\User;
use \Domain\Entities\Book;

class UserBookPurchase
{

    //region Attributes

    /**
     * @var string
     */
    private $id;

    /**
     * @var  \Domain\Entities\Book
     */
    private $book;

    /**
     * @var \Domain\Entities\User
     */
    private $user;

    //endregion


    //region Getters & Setters

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return $this;
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Book
     */
    public function getBook()
    {
        return $this->book;
    }

    /**
     * @param Book $book
     * @return $this;
     */
    public function setBook(Book $book = null)
    {
        $this->book = $book;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return $this;
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;
        return $this;
    }

    //endregion
}