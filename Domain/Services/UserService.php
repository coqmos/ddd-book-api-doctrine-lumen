<?php
namespace Domain\Services;

use Domain\Abstractions\AbstractDomainService;
use Domain\Contracts\Repository\UserRepositoryContract;
use Domain\Contracts\Service\UserServiceContract;

class UserService extends AbstractDomainService implements UserServiceContract
{

    public function __construct( UserRepositoryContract $repositoryContract)
    {
        parent::__construct($repositoryContract);
    }


}