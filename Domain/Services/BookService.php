<?php
namespace Domain\Services;

use Domain\Abstractions\AbstractDomainService;
use Domain\Contracts\Repository\BookRepositoryContract;
use Domain\Contracts\Service\BookServiceContract;

class BookService extends AbstractDomainService implements BookServiceContract
{

    public function __construct( BookRepositoryContract $repositoryContract)
    {
        parent::__construct($repositoryContract);
    }


}