<?php

$app->group(['prefix' => 'books','middleware' => []], function () use ($app) {
    $app->get('/', 'BookController@index');
    $app->post('/{bookId}/purchase', 'BookController@purchase');
});
