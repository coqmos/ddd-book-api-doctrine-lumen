<?php

$app->group(['prefix' => 'users','middleware' => []], function () use ($app) {
    $app->post('/', 'UserController@store');
    $app->get('/{userId}', 'UserController@show');
    $app->put('/{userId}', 'UserController@update');
    $app->delete('/{userId}', 'UserController@destroy');
    $app->get('/{userId}/purchase-history', 'UserController@purchaseHistory');
});
