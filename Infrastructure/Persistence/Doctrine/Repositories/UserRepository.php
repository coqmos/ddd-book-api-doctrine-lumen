<?php
namespace Infrastructure\Persistence\Doctrine\Repositories;

use Domain\Contracts\Repository\UserRepositoryContract;
use Domain\Entities\User;
use Infrastructure\Persistence\Doctrine\Repositories\AbstractRepository;
use Doctrine\ORM\EntityManager;

class UserRepository extends AbstractRepository implements UserRepositoryContract
{
    public function __construct(EntityManager $em, User $entity)
    {
        parent::__construct($em, $entity);
    }

}