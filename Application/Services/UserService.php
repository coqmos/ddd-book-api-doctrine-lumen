<?php

namespace Application\Services;


use Application\Core\Exceptions\ApplicationException;
use Domain\Contracts\Service\UserServiceContract;

class UserService
{
    private $modalityService;

    public function __construct(UserServiceContract $modalityService)
    {
        $this->modalityService = $modalityService;
    }

    public function find($modalityId)
    {
        return $this->modalityService->find($modalityId);
    }

    public function update($modalityId, $post)
    {
        return $this->modalityService->update($modalityId, $post);
    }

    public function create($post){
        return $this->modalityService->create($post);

    }


    public function delete($modalityId)
    {
        //TODO Remove purchase history
        return $this->modalityService->delete($modalityId);

    }

}