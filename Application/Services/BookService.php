<?php

namespace Application\Services;


use Application\Core\Exceptions\ApplicationException;
use Domain\Contracts\Service\BookServiceContract;

class BookService
{
    private $modalityService;

    public function __construct(BookServiceContract $modalityService)
    {
        $this->modalityService = $modalityService;
    }



    public function findAll($filter)
    {
        return $this->modalityService->findAllFiltered($filter);
    }

}