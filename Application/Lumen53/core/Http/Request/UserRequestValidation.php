<?php
/**
 * Created by PhpStorm.
 * User: davi
 * Date: 4/10/17
 * Time: 7:01 PM
 */

//validator documentation
//https://github.com/vlucas/valitron

namespace Application\Lumen53\Http\Request;

use Application\Lumen53\Http\Request\Validator;

class UserRequestValidation extends Validator{

    const updateRules = [
        'email' => ['email'],
        'name' =>  ['required','alpha'],
        'surname' =>  ['alpha'],
    ];

    const createRules = [
        'email' => ['required','email'],
        'name' =>  ['required','alpha'],
        'surname' =>  ['required','alpha']
    ];

    public function validateStore($post)
    {
        return $this->requestValidator($post,self::createRules);
    }

    public function validateUpdate($post){
        return $this->requestValidator($post,self::updateRules);
    }

}

