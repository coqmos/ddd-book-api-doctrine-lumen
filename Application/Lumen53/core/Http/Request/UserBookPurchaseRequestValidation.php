<?php
/**
 * Created by PhpStorm.
 * User: davi
 * Date: 4/10/17
 * Time: 7:01 PM
 */

//validator documentation
//https://github.com/vlucas/valitron

namespace Application\Lumen53\Http\Request;

use Application\Lumen53\Http\Request\Validator;

class UserBookPurchaseRequestValidation extends Validator{


    const createRules = [
        'user_id' => ['required','numeric'],
        'book_id' =>  ['required','numeric']
    ];

    public function validateStore($post)
    {
        return $this->requestValidator($post,self::createRules);
    }

}

