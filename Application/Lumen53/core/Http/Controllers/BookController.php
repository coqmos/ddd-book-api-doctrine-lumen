<?php
namespace Application\Lumen53\Http\Controllers;

use Application\Services\BookService;

class BookController extends Controller
{
    public function __construct(BookService $applicationService)
    {
        parent::__construct($applicationService);
    }

}