<?php
namespace Application\Lumen53\Providers;

use Illuminate\Support\ServiceProvider;

class UserServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('Domain\Contracts\Repository\UserRepositoryContract', 'Infrastructure\Persistence\Doctrine\Repositories\UserRepository');
        $this->app->bind('Domain\Contracts\Service\UserServiceContract', 'Domain\Services\UserService');
    }
}