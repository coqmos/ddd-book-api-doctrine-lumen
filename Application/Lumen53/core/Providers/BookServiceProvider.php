<?php
namespace Application\Lumen53\Providers;

use Illuminate\Support\ServiceProvider;

class BookServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('Domain\Contracts\Repository\BookRepositoryContract', 'Infrastructure\Persistence\Doctrine\Repositories\BookRepository');
        $this->app->bind('Domain\Contracts\Service\BookServiceContract', 'Domain\Services\BookService');
    }
}